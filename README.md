# README #

This README documents whatever steps are necessary to get your application up and running.

### What is this repository for? ###

## * Quick summary: ##
This repository contains the code for my current ongoing project of building a blog website on Laravel Framework. This project is about implementing what i learn about the framework in an web application. The application will continue to evolve over  the time.

## * Version: ##
Laravel: 5.4.21
PHP: 7.1.2
//* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

## * Summary of set up ##
Step 1: Install latest version of PHP
Step 2: Install composer on your computer
Step 3: Install latest version of Laravel on your computer 

## * Configuration ##
-> Configure your php.ini file (you can convert php.ini-development to php.ini)

{**I enabled this extensions:**
php_fileinfo.dll, php_gd2.dll, php_gettext.dll, php_ldap.dll, php_mbstring.dll,php_exif.dll, php_mysqli.dll, php_openssl.dll, php_pdo_mysql.dll, php_sockets.dll, php_xmlrpc.dll}

-> Configure mysql server

-> Configure .env file

## * Dependencies ## 
use "composer update" from the project directory and let the composer take care of the dependencies.

## * Database configuration ##
Migrations are given. Just run them and the schema will be created. then you can add posts and start exploring. I will add database seeding very soon. you can use "db_copy[posts]" to add some test data.

### Who do I talk to? ###

* Repo owner or admin: Tejas Rupade
email id: tejasrupade555@gmail.com