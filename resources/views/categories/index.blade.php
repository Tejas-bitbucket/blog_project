@extends('main')

@section('title', '| All Categories')

@section('content')

<div class="row">
	<div class="col-md-8">
		<h1>Categories</h1>
			<table class="table">
				<thead>
					<tr>
						<th>#</th>
						<th>Name</th>
					</tr>
				</thead>

				<tbody>
					@foreach ($categories as $category)
					<tr>
						<th>{{ $category -> id}}</th>
						<td>{{ $category -> name }}</td>
					</tr>
					@endforeach   
				</tbody>
			</table>
		</h1>
	</div><!--end of .col-md-8 -->

	<div class="col-md-3 col-md-offset-1">
		<div class="panel panel-info" style="position: fixed;margin-top: 50px; margin-left: 30px; text-align: center; border:2px dashed lightblue;">
			<div class="panel-heading">
				Create a Category
			</div>
			
			<div class="panel-body">
				<form action="{{ route('categories.store') }}" method="POST">
					{{csrf_field()}}
					
					<label class="form-spacing-top" style="text-align: left;"> Category Name:
					<input type="text" name="name" placeholder="category name please" class="form-control" style="margin-top: 8px;"></label>

					<input type="submit" value="Create New Category" class="btn btn-success form-spacing-top btn-block">

				</form>
			</div>

		</div>
	</div>

</div>

@stop